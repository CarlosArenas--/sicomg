angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope,localStorageService,sicosaServices,$ionicModal,$ionicPopover,$ionicPopup,$rootScope) {
  $scope.fullName = localStorageService.get('name');
  $scope.area = localStorageService.get('area');
  if($scope.area == 'Encargado de Obra'){
    $scope.hasAssignedWork = true;
  }
  sicosaServices.isLogged(localStorageService.get('id'),function(response){
    $scope.works = response.works;
  });


  $ionicModal.fromTemplateUrl('templates/worksModal.html', {
    id: '1', // We need to use and ID to identify the modal that is firing the event!
    scope: $scope,
    // backdropClickToClose: false,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.oModal1 = modal;
  });

  $ionicModal.fromTemplateUrl('templates/phaseMaterialModal.html', {
    id: '2', // We need to use and ID to identify the modal that is firing the event!
    scope: $scope,
    // backdropClickToClose: false,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.oModal2 = modal;
  });

  $ionicModal.fromTemplateUrl('templates/modal-edit-material.html', {
    id: '3',
    animation: 'slide-in-up',
    scope: $scope
  }).then(function(modal) {
    $scope.modalEditMaterial = modal;
  });

  $ionicModal.fromTemplateUrl('templates/modal-add-material.html', {
    id: '4',
    animation: 'slide-in-up',
    scope: $scope
  }).then(function(modal) {
    $scope.modalAddMaterial = modal;
  });


  $ionicModal.fromTemplateUrl('templates/modal-add-new-material.html', {
    id: '5',
    animation: 'slide-in-up',
    scope: $scope
  }).then(function(modal) {
    $scope.modalAddNewMaterial = modal;
  });




  $scope.openModal = function(e, id, index) {
    console.log(index);
    if (index == 1){
      $rootScope.currentWorkId = id;
      sicosaServices.getWork(id,function(response){
        $scope.workDetails = response;
        $scope.oModal1.show();
      });
      sicosaServices.getPhases({},function(response){
        $scope.allPhases = response;
        $scope.phases = response;
        for(var i=0; i<$scope.workDetails.phase.length; i++){
          for(var j=0; j<$scope.phases.length; j++){

            if($scope.workDetails.phase[i].id == $scope.phases[j].id){

              $scope.phases.splice(j,1);
            }
          }
        }
        console.log($scope.phases);
      });
    }else if(index == 2){

      sicosaServices.getMaterials({phase_id: id,work_id: $scope.currentWorkId}, function(response){
        $scope.materials = response;
        $scope.oModal2.show();
        
      });
    }else if(index == 3){
      sicosaServices.getMaterial(id, function(response){
        $scope.materialDetails = response;
        $scope.modalEditMaterial.show();
      });
    }else if(index == 4){
      $scope.popover.hide();
      //$scope.materialsList = response;
      $scope.modalAddMaterial.show();
      
    }else if(index == 5){
      //$scope.modalAddPhase.show();
      $scope.modalAddNewMaterial.show();
    }
  };

  $scope.closeModal = function(index) {
    if (index == 1){
      $scope.oModal1.hide();
    } else if(index == 2){
      $scope.oModal2.hide();
    } else if(index == 3){
      $scope.modalEditMaterial.hide();
    } else if(index == 4){
      $scope.modalAddMaterial.hide();
    } else if(index == 5){
      //$scope.modalPhaseMaterial.hide();
      $scope.modalAddNewMaterial.hide();

    }

  };


  $scope.toggleMaterial = function(mat) {
    if ($scope.isMaterialShown(mat)) {
      $scope.shownMaterial = null;
    } else {
      $scope.shownMaterial = mat;
    }
  };
  $scope.isMaterialShown = function(mat) {
    return $scope.shownMaterial === mat;
  };

  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    // $scope.modal.remove();
    $scope.oModal1.remove();
    $scope.oModal2.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

  $ionicPopover.fromTemplateUrl('templates/add-material-popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.showConfirm = function(mat) {
   var confirmPopup = $ionicPopup.confirm({
     title: 'Eliminar material',
     template: '¿Estás seguro de elimiar el producto ' + mat.name + '?'
   });

   confirmPopup.then(function(res) {
     if(res) {
        console.log(mat);
       $scope.phaseDetails.material.splice($scope.phaseDetails.material.indexOf(mat), 1);
     } else {
       //...
     }
   });
 };

  $scope.btnDelete = {
    showDelete: false,
    showDetails:true
  };

  $scope.deleteMaterial = function(mat) {
    $scope.showConfirm(mat);
  };

  $scope.$on('sendOpt',function(e,opt){
    sicosaServices.addPhaseToWork({work_id:$rootScope.currentWorkId,phase_id: opt.id}, function(response){
      $scope.openModal(e,$rootScope.currentWorkId,1)
    });
  });



})

.controller('ChatsCtrl', function($scope) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});


})

.controller('ChatDetailCtrl', function($scope, $stateParams) {

})

.controller('loginCtrl', function($scope, $stateParams,sicosaServices, $window) {
  $scope.username = '';
  $scope.password = '';
  $scope.signme = function(e){
      if($scope.username == '' || $scope.password == '' || $scope.username == null || $scope.password == null){
          alert("Por favor llena todos los campos");
          return 0;
      }else{
          sicosaServices.login({email: $scope.username, password: $scope.password},function(response){
              if(response.token != null){
                $window.location = '#/tab/dash';
              }

          });

      }
    };
})

.controller('AccountCtrl', function($scope,localStorageService, $window) {

  $scope.fullName = localStorageService.get('name');
  $scope.logout = function(e){
    localStorageService.clearAll();
    $window.location = '#/';
  };



});
