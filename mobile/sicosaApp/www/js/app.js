// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngResource','LocalStorageModule','ionic-modal-select'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    // var io = Ionic.io();
    // var push = new Ionic.Push({
    //   "onNotification": function(notification) {
    //     alert("Nueva Alerta");
    //   },
    //   "pluginConfig":{
    //     "android":{
    //       "iconColor": "#0000FF"
    //     }
    //   }
    // });
    // var details = {
    //   'email': 'theprocess.off@gmail.com',
    //   'password': 'Rock4ever'
    // };


    // var options = { 'remember': true };
    // Ionic.Auth.login('basic', options, details).then(authSuccess, authFailure);

    // var user  =  Ionic.User.current();
    // if (user.isAuthenticated()) {
    //   console.log(user);
    // } else {
    //   Ionic.Auth.signup(details).then(signupSuccess, signupFailure);
    // }

    // var callback = function (params) {
    //   push.saveToken(user);
    //   user.save();
    // };
    // push.register(callback);
    // push.register(function(token){
    //   console.log(token.token);
    // });
  });
})

.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

 

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    },
    resolve:{
      isLogged: function(){

      }
    }
  })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })
  .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl',
      resolve:{
            isLogged: function($window,localStorageService,$q){
                if(localStorageService.get('token') != null){
                    $window.location = '#/tab/dash'
                }
            },

        }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

}])




.constant("constants", {
        "baseUrl": "http://162.243.249.187:8088/",
        // "baseUrl": "http://localhost:1337/"
 });
