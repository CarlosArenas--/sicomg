angular.module('starter.services', [])

.factory('sicosaServices', function($resource,constants,localStorageService) {
  // Might use a resource here that returns a JSON array
      var baseUrl = constants.baseUrl;
    // Service logic
    // ...

    var login = function( params, callback ) {
        var Service = $resource(baseUrl+'login/');
	       Service.save(params).$promise.then(function(response) {
		        _loginResponse(response, callback);
		     }, _errorResponse);
    };

    var isLogged = function( params, callback ) {
        var Service = $resource(baseUrl+'user?id='+params);
	       Service.get().$promise.then(function(response) {
		        _successResponse(response, callback);
		     }, _errorResponse);
    };

    var getWork = function( params, callback ) {
        var Service = $resource(baseUrl+'work/'+params);
           Service.get().$promise.then(function(response) {
                _successResponse(response, callback);
             }, _errorResponse);
    };

    var getPhase = function( params, callback ) {
        var Service = $resource(baseUrl+'phase/'+params);
           Service.get().$promise.then(function(response) {
                _successResponse(response, callback);
             }, _errorResponse);
    };

    var getPhases = function( params, callback ) {
        var Service = $resource(baseUrl+'phase/');
           Service.query().$promise.then(function(response) {
                _successResponse(response, callback);
             }, _errorResponse);
    };

    var addPhaseToWork = function( params, callback ) {
          var Service = $resource(baseUrl+'work/'+params.work_id+ '/phase/'+ params.phase_id);
          Service.save().$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };

    var getMaterial = function( params, callback ) {
        var Service = $resource(baseUrl+'material/'+params);
           Service.get().$promise.then(function(response) {
                _successResponse(response, callback);
             }, _errorResponse);
    };

    var getMaterials = function( params, callback ) {
        var Service = $resource(baseUrl+'material?phase='+params.phase_id +'&work='+params.work_id);
        Service.query().$promise.then(function(response) {
            _successResponse(response, callback);
         }, _errorResponse);
    };
    var _loginResponse = function(response, callback){

        localStorageService.set('token', response.token);
        localStorageService.set('name',response.user.first_name + ' ' + response.user.last_name );
        localStorageService.set('id', response.user.id);
        localStorageService.set('area',response.user.area);
        localStorageService.set('email',response.user.email);
        localStorageService.set('password',response.user.password);
        //localStorageService.set(user, response.user);
        return callback( response );

    };


    var _successResponse = function(response, callback){
            var details = {
                'email': localStorageService.get('email'),
                'password': localStorageService.get('password')
            };

            //  var options = { 'remember': true };
            //     var user  =  Ionic.User.current();
            //     console.log(user);
            //     var successLogin = function(){
            //     return true;
            // };
            // var failedLogin = function(){
            //     console.log("fail Log");
            //     return Ionic.Auth.signup(details).then(signupSuccess, signupFailure);
            // }
            // if (user.isAuthenticated()) {
            //     console.log(user);
            // } else {
            //     Ionic.Auth.signup(details).then(signupSuccess, signupFailure);
            // }
            //     Ionic.Auth.login('basic', options, details).then(successLogin, failedLogin);


                //localStorageService.set(user, response.user);
            return callback( response );
    };

    var _errorResponse = function(response){
        console.log("on error");
        $window.location = '#/';
    }
    // Public API here
    return {
      login:login ,
      isLogged: isLogged,
      getWork: getWork,
      getPhase: getPhase,
      getPhases: getPhases,
      addPhaseToWork: addPhaseToWork,
      getMaterial: getMaterial,
      getMaterials: getMaterials
    };

});
