/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    
    sendUserPass: function(req,res){
       sails.hooks.email.send(
        "testEmail",
        {
            recipientName: req.param('first_name'),
            senderName: "SICOSA",
            password: req.param('password')
            
        },
        {
            to: req.param('email'),
            subject: "Contraseña del Sistema"
        },
        function(err) {
            if (err) {
                return res.negotiate(err);
            }
            return res.json({error:false }); 
        }
        )
       
    }
    
	
};

