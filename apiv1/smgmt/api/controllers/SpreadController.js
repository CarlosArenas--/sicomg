/**
 * SpreadController
 *
 * @description :: Server-side logic for managing spreads
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	sendPush:function(req,res){
		console.log(req.body);
		var users = req.body.users.toString();

		Spread.query("SELECT device_token FROM  user  WHERE id in ("+users +")",function(err,push){
			//console.log(users); 
			var pusher = [];
			for(var i=0; i<push.length; i++){
					pusher.push(push[i].device_token)
			}
			//pusher.push('cZvA1W2q7AE:APA91bHhp1NK9iMZkALQYZ6c8_wKodxBuZjx3o1J7EbHxM6QE3wn6ALQC6b7SSmb1W4YiF8fTF0HFVghBRF1eiEWlSEehKMJ_enBouucUPDioGxqrXLHR-7M9Wc8lv_y2j_zdh5JPdW_');
			if (err) {
				 return res.negotiate(err);
		 }
		 sails.services.pushnotification.sendGCMNotification(pusher, {
				 data : {
						 key1 : 'message1',
						 key2 : 'message2',
				 },
				 notification: {
						 sound:'default',
						 title: "Documentos",
						 icon: "ic_launcher",
						 body: "Has sido asignado a un nuevo documento"+ req.body.name
				 }
				 }, true, function (err, results)
				 {
						 console.log(err, results);
				 });
				 return res.json({error:false, response: push});
		});
	},

	saveSpread:function(req,res){
        console.log(req.body);
        Spread.create({name: req.body.name,
            author: req.body.author,
            url: req.body.url,
            subject: req.body.subject}).exec(function (err, records) {
            if(err){
                return res.negotiate(err);
            }

            User.findOne(records.id).exec(function(err, user) {
            if(err) // handle error

            // Queue up a record to be inserted into the join table
            user.users.add(req.body.users);
            // Save the user, creating the new pet and associations in the join table
            user.save(function(err) {});
            return res.json({error:false, response: records});
            });
        });
        sails.services.pushnotification.sendGCMNotification("cZvA1W2q7AE:APA91bHhp1NK9iMZkALQYZ6c8_wKodxBuZjx3o1J7EbHxM6QE3wn6ALQC6b7SSmb1W4YiF8fTF0HFVghBRF1eiEWlSEehKMJ_enBouucUPDioGxqrXLHR-7M9Wc8lv_y2j_zdh5JPdW_", {
        data         : {
        	key1 : 'message1',
        	key2 : 'message2'
        },
        notification: {
        	title: "Documento Sicosa",
        	icon: "ic_launcher",
        	body: "Se ha creado un documento"
        }
        }, true, function (err, results)
        {
            console.log(err, results);
        });
    }
};
