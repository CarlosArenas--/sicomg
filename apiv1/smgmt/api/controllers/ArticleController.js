/**
 * ArticleController
 *
 * @description :: Server-side logic for managing articles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getArticles:function(req, res) {
         Article.query("SELECT device_token FROM  user  WHERE  area =  'Administrador'",function(err,push){
             if (err) {
                return res.negotiate(err);
            }
            console.log(push);
            sails.services.pushnotification.sendGCMNotification(push, {
            data         : {
                key1 : 'message1',
                key2 : 'message2'
            },
            notification: {
                title: "Nuevo Articulo",
                icon: "ic_launcher",
                body: "Se ha agregado un Nuevo Artículo"
            }
            }, true, function (err, results)
            {
                console.log(err, results);
            });
            return res.json({error:false , response:push});
         });
        
    },
	getArticlesUsers: function(req,res){
        Article.query("SELECT u.id as user_id, a.id as article_id, a.name as article_name, a.serie, au.createdAt as fecha_prestamo, CONCAT_WS(' ', u.first_name, u.last_name) as user_name, u.area as puesto, au.comment_before, au.comment_after, a.specs, a.accesories, au.sign FROM article a INNER JOIN articleuser au ON a.id = au.article INNER JOIN user u ON au.user = u.id", function(err, art){
           if (err) {
                return res.negotiate(err);
            }
            return res.json({error:false , response:art});
        });
   },
	 createRequest: function(req,res){
		 	Article.query("INSERT INTO `articleuser`(`user`, `article`, `comment_before`,  `updatedAt`) VALUES ("+req.params.id_user+","+req.params.id_article+","+req.params.comment_before+",NOW())",function(err,article_req){
				if(err) {
					return res.negotiate(err);
				}
				return res.json({error:false, response:'Solicitud creada con exito'});
			})
	 }

};
