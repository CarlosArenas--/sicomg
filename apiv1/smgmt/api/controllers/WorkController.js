/**
 * WorkController
 *
 * @description :: Server-side logic for managing works
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    getWorks:function(req, res) {
        Work.find().exec(function(err,works){
            if(err){
                return res.negotiate(err);
            }
            return res.json({error:false, response: works});
        });
        // sails.services.pushnotification.sendGCMNotification("22e8a612-e4a8-479c-ac9f-f71c8608a0cf", {
        // data         : {
        // key1 : 'message1',
        // key2 : 'message2'
        // },
        // notification: {
        // title: "Hello, World",
        // icon: "ic_launcher",
        // body: "This is a notification that will be displayed ASAP."
        // }
        // }, true, function (err, results)
        // {
        //     console.log(err, results);
        // });
    },
	getWorksDetails: function(req, res) {
        Work.find().populateAll().exec(function(err,works){
            if(err){
                return res.negotiate(err);
            }
            return res.json({error:false, response: works});
        });
    },
     getWorksById: function(req,res){
        Work.find({id: req.params.id}).populateAll().exec(function (err, work){
            if (err) {
                return res.negotiate(err);
            }
            return res.json({error:false , response:work});
        });
    },
    
    removeWorkPhase: function(req,res){
        Work.query('REMOVE FROM phase_work__work_phase WHERE id = '+ req.params.id).exec(function(err, work){
           if (err) {
                return res.negotiate(err);
            }
            return res.json({error:false , response:work}); 
        });
    }
    

};

