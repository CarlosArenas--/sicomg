/**
 * EmployeeController
 *
 * @description :: Server-side logic for managing employees
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	saveEmployee:function(req,res){

        Employee.create(req.body).exec(function (err, records) {
            if(err){
                return res.negotiate(err);
            }

            return res.json({error:false, response: records});

            });
           Employee.query("SELECT device_token FROM  user  WHERE  area =  'Administrador'",function(err,push){

             var pusher = [];
             for(var i=0; i<push.length; i++){
                 pusher.push(push[i].device_token)
             }
             //pusher.push('cZvA1W2q7AE:APA91bHhp1NK9iMZkALQYZ6c8_wKodxBuZjx3o1J7EbHxM6QE3wn6ALQC6b7SSmb1W4YiF8fTF0HFVghBRF1eiEWlSEehKMJ_enBouucUPDioGxqrXLHR-7M9Wc8lv_y2j_zdh5JPdW_');
             if (err) {
                return res.negotiate(err);
            }
            sails.services.pushnotification.sendGCMNotification(pusher, {
                data : {
                    key1 : 'message1',
                    key2 : 'message2',
                },
                notification: {
                    sound:'default',
                    title: "Empleados",
                    icon: "ic_launcher",
                    body: "Se ha dado de alta un empleado con nombre"+ req.body.name
                }
                }, true, function (err, results)
                {
                    console.log(err, results);
                });

           });

    }
};
