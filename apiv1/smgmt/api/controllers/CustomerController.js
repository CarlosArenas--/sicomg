/**
 * CustomerController
 *
 * @description :: Server-side logic for managing customers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getCustomers: function(req, res) {
        Customer.find().populate('works').exec(function(err,customers){
            if(err){
                return res.negotiate(err);
            }
            return res.json({error:false, response: customers});
        });
    }
   
    
};

