module.exports = {
  getWorks: function(next) {
    Work.find().exec(function(err, works) {
      if(err) throw err;
      next(works);
    });
  }
};