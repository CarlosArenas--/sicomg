/**
 * Requisition.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    signature:{
      type: 'string'
    },
    sign_by:{
      type: 'string'// some has to approve this
    },
    author:{
      type: 'string'
    },
    code:{
      type: 'string'
    },
    product:{
      type: 'string'
    },
    description:{
      type: 'text'
    },
    unit:{
      type: 'string'
    },
    qty_requisited:{
      type: 'int'
    },
    qty_required:{
      type: 'int'
    },
    date_required:{
      type: 'date'
    },
    departure:{
      type: 'string'
    },
    pending:{
      type: 'string'
    },
    status:{
      type:'string' //when status change to sucess Contabilidad could see the req
    },
    user:{
      model:'user'
    },
    work:{
      model:'work'
    }

  }
};
