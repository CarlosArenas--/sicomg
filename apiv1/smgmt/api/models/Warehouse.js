/**
 * Warehouse.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name:{
      type: 'string'
    },
    description:{
      type: 'string'
    },
    units:{
      type: 'string'
    },
    // total:{
    //   type:'int'
    // },
    // stock:{
    //   //the total minus the count of the by loans via tools
    //   type: 'int'
    // },
    //marca
    //modelo
    brand:{
      type:'string'
    },
    model:{
      type:'string'
    },
    no_serie:{
      type: 'string',
      unique: true
    },
    state:{
      type: 'string'
    },
    status:{
      type: 'string'
    },
    // loans: {
    //   collection: 'loan',
    //   via: 'tools',
    //   dominant: true
    // },
    work:{
      model: 'work'
    },
    assigned_to:{
      type: 'string'
    },
    comments:{
      type: 'text'
    },
    category:{
      type:'string'
    }
  }
};
