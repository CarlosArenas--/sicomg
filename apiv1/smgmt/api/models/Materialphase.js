/**
 * Materialphase.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
     phase:{
        model:'phase'
    },
    material:{
        model:'material'
    },
    units:{
         type: 'string'
    },
    qty:{
        type: 'float'
    } 
  }
};

