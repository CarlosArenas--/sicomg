/**
 * Loan.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    work:{
      model: 'work'
    },
    notes:{
      type:'string'
    },
    assigned_to:{
      type:'string'
    },
    no_serie:{
      type: 'string',
      unique: true
    },
    qty:{
      type: 'int'
    }
    // tools: {
    //   collection: 'warehouse',
    //   via: 'loans'
    // }
  }
};

//Do a count by tools


