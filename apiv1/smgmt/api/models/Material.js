/**
 * Material.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id:{
        type: 'integer',
        primaryKey: true
    },
    name:{
        type: 'string'
    },
    price:{
        type: 'integer'
    },
    description:{
        type:'text'
    },
    units:{
         type: 'string'
    },
    qty:{
        type: 'float'
    },
    work:{
        model: 'work'
    },
    supplier:{
        collection:'supplier',
        via: 'material',
        through: 'materialsupplier'
    },
    phase:{
      model: 'phase'
    },
    created_by:{
        type: 'string'
    }

  }
};
