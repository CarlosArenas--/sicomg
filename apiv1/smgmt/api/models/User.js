/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
      area: {
          type: 'string'
      },
      photo:{
          type: 'string'
      },
      last_name:{
          type: 'string'
      },
      first_name:{
          type: 'string'
      },
      birthday:{
          type: 'date'
      },
      device_token:{
          type: 'string'
      },
      works:{
          collection:'work',
          via: 'users',
          through: 'userwork'
      },
	  articles:{
		collection: 'article',
		via: 'user',
		through: 'articleuser'
	  },
      spreads:{
        collection: 'spread',
        via: 'users'
      }
      // email:{
      //   type: 'string',
      // },
      // password:{
      //   type: 'string'
      // }
  }
};
