/**
 * Article.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
	 users:{
      collection: 'user',
      via: 'article',
      through: 'articleuser'
    },
	name:{
		type: 'string'
	},
	serie:{
		type:'string'
	},
	specs:{
		type:'string'
	},
	accesories:{
		type: 'text'
	},
	status:{
		type:'string'
	}
  }
};

