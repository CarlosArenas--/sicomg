/**
 * Phase.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
      id:{
        type: 'integer',
        primaryKey: true,
      },
      name:{
          type: 'string'
      },
      description:{
          type: 'text'
      },
      status:{
          type: 'string'
      },
      work:{
         collection: 'work',
         via: 'phase'
      },
      material:{
         collection:'material',
         via:'phase'
     }

  }
};
