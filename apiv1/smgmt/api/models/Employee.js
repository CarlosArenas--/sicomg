/**
 * Employee.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name:{
      type:'string'
    },
    position:{
      type:'string'
    },
    date_of_admission:{
      type:'date'
    },
    imss:{
      type:'string'
    },
    birthday:{
      type:'date'
    },
    place_of_birth:{
      type: 'string'
    },
    curp:{
      type: 'string'
    },
    rfc:{
      type: 'string'
    },
    address:{
      type: 'string'
    },
    suburb:{
      type:'string'
    },
    municipality:{
      type: 'string'
    },
    zipcode:{
      type: 'string'
    },
    infonavit:{
      type: 'string'
    },
    fonacot:{
      type: 'string'
    },
    photo:{
      type: 'string'
    },
    work:{
      model:'work'
    },
    failure:{
      type: 'integer'
    },
    category:{
      type: 'string'
    }
  }
};

