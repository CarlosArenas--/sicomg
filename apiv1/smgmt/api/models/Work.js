/**
 * Work.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
        id:{
           type: 'integer',
           primaryKey: true,
        },
        title:{
            type:'string'
        },
        date:{
            type: 'string'
        },
        award:{
            type:'string'
        },
        photo:{
            type:'string'
        },
        mts:{
            type:'integer'
        },
        status:{
          type: 'string'
        },
        customer:{
            model: 'customer'
        },
        lat:{
            type: 'string'
        },
        lon:{
            type: 'string'
        },
        state:{
            type: 'string'
        },
        description:{
            type: 'text'
        },
        zip:{
            type: 'string'
        },
        material:{
            model: 'work'
        },
        type:{
            type:'string'
        },
        phase:{
            collection: 'phase',
            via: 'work',
            dominant: true
        },
        address:{
            type: 'string'
        },
        contract_type:{
            type:'string'
        },
        users:{
            collection: 'user',
            via: 'works',
            through: 'userwork'
        },
        spreads:{
            collection: 'spread',
            via: 'work'
        },
        photos:{
            collection:'photo',
            via:'work'
        }

  }
};
