//in seed/test/UserSeed.js 
var faker = require('faker');
 
//object to seed 
//in User model 
module.exports = {
    title: faker.company.catchPhrase(),
    state: faker.address.state(),
    description: faker.company.catchPhraseDescriptor(),
};