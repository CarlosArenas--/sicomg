//in seed/test/UserSeed.js 
var faker = require('faker');
 
//object to seed 
//in User model 
module.exports = {
    name: faker.commerce.productName(),
    price: faker.commerce.price(),  
    description: faker.commerce.productMaterial()
};