//in seed/test/UserSeed.js 
var faker = require('faker');
 
//object to seed 
//in User model 
module.exports = {
    title: faker.random.words(),
    customer: faker.random.number({
        'min': 1,
        'max': 15
    }),
    date: faker.date.past(),
    description: faker.company.catchPhraseDescriptor(),
    location: faker.random.number({
        'min': 1,
        'max': 15
    })
    
};