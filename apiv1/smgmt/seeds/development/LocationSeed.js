//in seed/test/UserSeed.js 
var faker = require('faker');
 
//object to seed 
//in User model 
module.exports = {
    lat: faker.address.latitude(),
    lon: faker.address.longitude(),
    address: faker.address.streetAddress(),
    zip: faker.address.zipCode(),
    description: faker.company.catchPhraseDescriptor(),
    state: faker.address.state()
    
};