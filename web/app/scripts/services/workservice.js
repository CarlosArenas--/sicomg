'use strict';

/**
 * @ngdoc service
 * @name sicosaApp.workService
 * @description
 * # workService
 * Factory in the sicosaApp.
 */
angular.module('sicosaApp')
  .factory('workService', function ($resource) {
    // Service logic
    // ...
    var getWorks = function( params, callback ) {
        var Service = $resource('http://localhost:1337/work/getWorks');
	       Service.get().$promise.then(function(response) {
		     _successResponse(response, callback);
		}, console.log("error"));
    };
    
    var _successResponse = function(response, callback){
        return callback( response );
    }

    // Public API here
    return {
      getWorks: getWorks
    };
  });
