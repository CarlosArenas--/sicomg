'use strict';

/**
 * @ngdoc service
 * @name sicosaApp.locationService
 * @description
 * # locationService
 * Factory in the sicosaApp.
 */
angular.module('sicosaApp')
  .factory('locationService', function ($resource) {
    
    var getLocations = function( params, callback ) {
        var Service = $resource('http://localhost:1337/location/getLocations');
	       Service.get().$promise.then(function(response) {
		     _successResponse(response, callback);
		});
    };
    
    var _successResponse = function(response, callback){
        return callback( response );
    }

    // Public API here
    return {
      getLocations: getLocations
    };
  });
