'use strict';

/**
 * @ngdoc overview
 * @name sicosaApp
 * @description
 * # sicosaApp
 *
 * Main module of the application.
 */
angular
  .module('sicosaApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angular-toArrayFilter'
  ])
  .config(function ($routeProvider,$locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/works', {
        templateUrl: 'views/works.html',
        controller: 'WorksCtrl',
        controllerAs: 'works'
      })
      .otherwise({
        redirectTo: '/'
      });
      
      // use the HTML5 History API
      //$locationProvider.html5Mode(true);
  });
