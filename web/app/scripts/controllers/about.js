'use strict';

/**
 * @ngdoc function
 * @name sicosaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sicosaApp
 */
angular.module('sicosaApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
