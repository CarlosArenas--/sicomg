'use strict';

/**
 * @ngdoc function
 * @name sicosaApp.controller:WorksCtrl
 * @description
 * # WorksCtrl
 * Controller of the sicosaApp
 */
angular.module('sicosaApp')
  .controller('WorksCtrl', function ($scope,workService,locationService) {
    $scope.init = function(){
        workService.getWorks({},function(response){
            console.log(response);
            $scope.works = response.response;
        });
        locationService.getLocations({},function(response){
           console.log(response); 
        });
        
    };
    
    $scope.init();
  });
