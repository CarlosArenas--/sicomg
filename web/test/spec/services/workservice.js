'use strict';

describe('Service: workService', function () {

  // load the service's module
  beforeEach(module('sicosaApp'));

  // instantiate service
  var workService;
  beforeEach(inject(function (_workService_) {
    workService = _workService_;
  }));

  it('should do something', function () {
    expect(!!workService).toBe(true);
  });

});
