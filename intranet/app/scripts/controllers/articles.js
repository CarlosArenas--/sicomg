'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:ArticlesCtrl
 * @description
 * # ArticlesCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
	.controller('ArticlesCtrl', function ($scope,userServices,articlesListServices) {
		$scope.init = function(){
			userServices.getUsers({},function(response){
				$scope.users = response;
			});
			articlesListServices.getArticles({},function(response){
				$scope.articles = response;
			});
			articlesListServices.getArticlesUsers({},function(response){
				$scope.articlesUsers = response.response;

			});
			$scope.$watch(function(){
				$(".select-lists").select2({
					placeholder: 'Seleccione',
					allowClear:true,
					theme: 'classic'
				});
			});
		};
		$('#articleTabs a').click(function (e) {
			e.preventDefault()
			$(this).tab('show')
		});

		$scope.newArticle = function(e){
			$scope.articleData = {};
			$scope.isEdit = false;
		};

		$scope.editArticle = function(e,article){
			$scope.articleData = article;
			$scope.isEdit = true;
		};

		$scope.saveArticleData = function(e){
			$scope.articleData.status = 'Disponible';
			articlesListServices.saveArticle($scope.articleData,function(response){
				$scope.hideModals();
				$scope.articles.push(response);
			});
		};

		$scope.updateArticle = function(e){
				articlesListServices.updateArticle($scope.articleData,function(response){
					$scope.hideModals();
				});
		};

		$scope.newRequestArticle = function(e){
			$scope.reqArticleData = {};
			$scope.isEdit = false;
		};

		$scope.editRequestArticle = function(e,req_article){
			$scope.reqArticleData = req_article;
			$scope.isEdit = true;
		};

		$scope.viewCommentsRequest = function(e,c_b, c_a){
			$scope.current_c_b = c_b;
			$scope.current_c_a = c_a;
			$scope.isEdit = true;
		};

		$scope.removeArtModal = function(e,art){
			$scope.currentRemoveArt = art;
		};
		$scope.deleteArticle = function(e, id){
			articlesListServices.removeArticle(id,function(reponse){
				// for(var i = 0; i<	$scope.articles.length; i++){
				// 	if($scope.articles[i].id == id){
				// 		$scope.articles.splice(i,1);
				// 	}
				// }
				$('#deleteArticleModal').modal('hide');
				$scope.init();
			});
		};

		$scope.hideModals = function(e){
			$('#newArticle').modal('hide');
		};

		$scope.saveNewArtRequest = function(e){
			articlesListServices.assignArticleToUser({user: $scope.reqArticleData.user_id,
				article: $scope.reqArticleData.article_id,
				comment_before: $scope.reqArticleData.comment_before},function(response){
					articlesListServices.updateArticle({id:$scope.reqArticleData.article_id,status:'Ocupado'},function(response){
						$scope.init();
						$('#newRequestArticle').modal('hide');
					});


			});
		};

		$scope.init();
	});
