'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:EstimatorCtrl
 * @description
 * # EstimatorCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('EstimatorCtrl', function ($scope,workServices,materialService,NgTableParams) {
    
    $scope.init = function(){
      $scope.currentMaterial = {};
      workServices.getWorks({},function(response){
           $scope.works = response.response;

      });
    };
    
    
    
    
    $scope.openMaterials = function(e){
      materialService.getMaterials({},function(response){
        $scope.materials = response;
        
        
      });
      
    };
    
    $scope.saveMaterial = function(e){
      materialService.saveMaterial($scope.currentMaterial,function(response){
        $('#materialsModal').modal("hide");
        $scope.currentMaterial = {};
      });
    }
    
    $scope.init();
    
  });
