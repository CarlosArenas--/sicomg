'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:ReportsCtrl
 * @description
 * # ReportsCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('ReportsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
