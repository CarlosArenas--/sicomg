'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:InventoryCtrl
 * @description
 * # InventoryCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('InventoryCtrl', function ($scope,inventoryServices) {
    $scope.init = function() {
        inventoryServices.getWares({},function(response){
            for(var i=0; i<response.length; i++){
                response[i].total = parseInt(response[i].total);
                response[i].stock = parseInt(response[i].stock);
            }
            $scope.wares = response;
        });
    };
    $scope.removeItem = function removeItem(e,row) {
        var index = $scope.wares.indexOf(row);
        if (index !== -1) {
            $scope.wares.splice(index, 1);
        }
    };
    $scope.updateItem = function(e,row){
        inventoryServices.updateWare(row, function(response){
            console.log(response);
        });
    };

    $scope.init(); 
  });
