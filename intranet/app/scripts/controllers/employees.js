'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:EmployeesCtrl
 * @description
 * # EmployeesCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('EmployeesCtrl', function ($scope, employeeServices) {
    $scope.init = function(){
      $scope.isChange = false;  
      $scope.currentEmployee = {};
      employeeServices.getEmployees({},function(response){
        console.log(response);
        $scope.employees = response;
      });
    };
    
    $scope.saveEmployee = function(e){
      employeeServices.saveEmployee($scope.currentEmployee, function(response){
        $scope.employees.push(response);
        $('#addEmployeeModal').modal("hide");
      });
    };
    
    
    $scope.openModal = function(e,size){
      $scope.currentEmployee = {};
      $scope.isChange = false;
    };
    
    $scope.editEmployee = function(e, employee){
      $scope.isChange = true;
      $scope.currentEmployee = employee;
      //$('#addEmployeeModal').modal("show");
      
    };
    
    $scope.changeDataEmployee = function(e){
      employeeServices.updateEmployee($scope.currentEmployee,function(response){
        $('#addEmployeeModal').modal("hide");
      });
    }
    
    $scope.removeEmployee = function(e,id){
      if(confirm('Estas seguro de querer eliminar este registro?')){
        employeeServices.removeEmployee(id, function(response){
          for(var i=0; i<$scope.employees.length; i++){
            if($scope.employees[i].id == id){
              $scope.employees.splice(i,1);
            }
          }  
        });
      }
    };
    
    $scope.uploadFile = function(file){
      console.log($('#image')[0].currentSrc);
    };
    
    $scope.printEmployee = function(e){
      window.print();
      return false;
    };
    
    $scope.init();
  });
