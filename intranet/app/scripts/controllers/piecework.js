'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:PieceworkCtrl
 * @description
 * # PieceworkCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('PieceworkCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
