'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:ReportdetailsCtrl
 * @description
 * # ReportdetailsCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('ReportdetailsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
