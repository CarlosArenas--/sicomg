'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:RequisitionsCtrl
 * @description
 * # RequisitionsCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('RequisitionsCtrl', function ($scope,requisitionServices) {
    $scope.init = function(){
      requisitionServices.getReqs({},function(response){   
        console.log(response);
        for(var i = 0; i<response.length; i++){
         if(response[i].date_required)
         response[i].date_required = response[i].date_required.substring(0, response[i].date_required.indexOf('T'));
         response[i].date_required = new Date(response[i].date_required);

        }
        $scope.requisitions = response;
      });
    };

    $scope.saveReq = function(e){
      $scope.req.status = "A"; //status not approved yet
      requisitionServices.saveReq($scope.req,function(response){
        $('#newReqModal').modal('hide');
        $scope.requisitions.push(response);
      });
    };

    $scope.removeReq = function(e,id){
      requisitionServices.removeReq(id,function(response){
          for(var i=0; i<$scope.requisitions.length; i++){
            if($scope.requisitions[i].id == id){
              $scope.requisitions.splice(i,1);
            }
          }
      });
    }

    $scope.init();
  });
