'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('MainCtrl', function ($scope,authService,localStorageService) {
    $scope.init = function() {
       $scope.name = localStorageService.get('name');
       $scope.area = localStorageService.get('area');
       $(function () {
         $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=usdeur.json&callback=?', function (data) {
            $('#container').highcharts({
                chart: {
                    zoomType: 'x'
                },
                title: {
                    text: 'Productividad'
                },
                subtitle: {
                    text: document.ontouchstart === undefined ?
                            'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Exchange rate'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 1,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },

                series: [{
                    type: 'area',
                    name: 'USD to EUR',
                    data: data
                }]
            });
        });
      });
      $(function () {
        $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Reportes por Persona'
        },
        subtitle: {
            text: 'Sicosa'
        },
        xAxis: {
            categories: [
                'Requisición',
                'Materiales',
                'Inventario',
                'Documentos Colaborativos',

            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Aprovado'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Claudio',
            data: [49.9, 71.5, 106.4, 129.2]

        }, {
            name: 'Andrea',
            data: [83.6, 78.8, 98.5, 93.4]

        }, {
            name: 'London',
            data: [48.9, 38.8, 39.3, 41.4]

        }, {
            name: 'Berlin',
            data: [42.4, 33.2, 34.5, 39.7]

        }]
    });
    });
    };

    $scope.init();

  });
