'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('menuCtrl', function ($scope,localStorageService,$window,bcrypt,userServices) {
      $scope.logout = function(){
         localStorageService.clearAll();
         $window.location = '#/';   
      };
      $scope.changePassword = function(e){
        if($scope.newPassword == '' || $scope.newPassword == null){
          alert("elige una contraseña");
          return 0;
        }else{
          userServices.updateUser({
                id: localStorageService.get('id'),
                password: bcrypt.hashSync($scope.newPassword)
                },function(response){
                $('#changePswModal').modal("hide");
          })
        }
      };
      
  });
