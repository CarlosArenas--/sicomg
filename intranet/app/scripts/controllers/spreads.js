'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:SpreadsCtrl
 * @description
 * # SpreadsCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('SpreadsCtrl', function ($scope, spreadServices, workServices,localStorageService, userServices) {
    $scope.init = function(){
      $scope.selectedWork = {originalObject:{}};
      $scope.spread = {};
      spreadServices.getSpreads({},function(response){
        $scope.spreads = response;
        // for(var i=0; i<$scope.spreads.length; i++){
        //   for(var j=0; j<$scope.spreads[i].users.length; j++){

        //     if($scope.spreads[i].users == []){
        //       $scope.hasAccessToDoc = true;
        //     }
        //   }
        // }
       $scope.idUser = localStorageService.get('id');
      });
      workServices.getWorks({},function(response){
        $scope.works = response.response;
       });
       userServices.getUsers({},function(response){
         $scope.users = response;
         console.log(response);
       });
       $scope.$watch(function(){
        $(".js-example-basic-multiple-users").select2();
      });
    };

    $scope.saveSpread = function(){
      if($scope.spread.name == '' || $scope.spread.name == null){
        alert("por favor completa el campo de título");
        return 0;
      }

      $scope.spread.work = $scope.selectedWork.originalObject.id;

      $scope.spread.author = localStorageService.get('name');
      $scope.spread.users.push(localStorageService.get('id').toString());
      $scope.spread.url = $scope.spread.name +''+Math.floor(Date.now() / 1000);
      spreadServices.saveSpread($scope.spread,function(response){
        $('#addSpread').modal("hide");
        $scope.spreads.push(response);
      });
      spreadServices.sendPush($scope.spread,function(response){

      });
    };

    $scope.removeSpread = function(e,id){
      if(confirm('Estas seguro de querer eliminar a esto?')){
        spreadServices.removeSpread(id,function(response){
          for(var i=0; i<$scope.spreads.length; i++){
              if($scope.spreads[i].id == id){
                $scope.spreads.splice(i,1);
              }
            }
        });
      }
    };

    $scope.init();
  });
