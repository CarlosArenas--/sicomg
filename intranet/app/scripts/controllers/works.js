'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:WorksCtrl
 * @description
 * # WorksCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('WorksCtrl', function ($scope,workServices,$window,NgMap, $timeout, cfpLoadingBar,Upload,$q) {
    $scope.init = function(){
        $scope.start();
        $scope.isChange = false;      
        $scope.currentWork = {};
        workServices.getWorks({},function(response){
           $scope.works = response;
           $scope.complete();
        });
        $('#addWorkModal').on('show.bs.modal', function(e, size) {
          console.log(map);

            google.maps.event.trigger(map, 'resize');
            //$scope.$apply();
            //return map.setCenter(latLng);            
        });
    };
    
    $scope.goToWork = function(e, id){
        $window.location = '#/works/'+id;
    };
    
    $scope.openModal = function(e,size){
      $scope.currentWork = {};
      $scope.isChange = false;
      //e.preventDefault();
      //e.stopImmediatePropagation();
      $scope.showModal = true;
      
    };
    
    $scope.editWork = function(e, work){
      $scope.isChange = true;
      $scope.currentWork = work;
      $('#addWorkModal').modal("show");
      
    };
    
    $scope.changeDataWork = function(e){
      if($scope.file){
      Upload.upload({
            url: 'http://intranet.sicosa.mx/upload/upload.php',
            data: {file: $scope.file}
        }).then(function (resp) {
            var deferred = $q;
            //$scope.works.photo = resp.config.data.file.name;
            //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            $scope.photo = resp.data.tmp_name.replace('/tmp/', '');
            //console.log($scope.photo);
            $scope.currentWork.photo = $scope.photo;
            deferred.resolve();
            
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              console.log($scope.file);
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
          }
      $timeout(function(){
        workServices.updateWork($scope.currentWork,function(response){
          $('#addWorkModal').modal("hide");
        });
      },2000)
    }
    
    $scope.removeWork = function(e, id){
      if(confirm('Estas seguro de querer eliminar este registro?')){
        workServices.removeWork(id, function(response){
          for(var i=0; i<$scope.works.length; i++){
            if($scope.works[i].id == id){
              $scope.works.splice(i,1);
            }
          }  
        });
      }
  };
  
  $scope.getCurrentLocation = function(e){
    
      //console.log(e.latLng);
      $scope.currentWork.lat = e.latLng.lat();
      $scope.currentWork.lon = e.latLng.lng();
      
  };
    
    $scope.saveWork = function(e){
      console.log($scope.currentWork);
      $scope.currentWork.customer = 1;
      Upload.upload({
            url: 'http://intranet.sicosa.mx/upload/upload.php',
            data: {file: $scope.file}
        }).then(function (resp) {
            var deferred = $q;
            //$scope.works.photo = resp.config.data.file.name;
            //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            $scope.photo = resp.data.tmp_name.replace('/tmp/', '');
            //console.log($scope.photo);
            $scope.currentWork.photo = $scope.photo;
            deferred.resolve();
            
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              console.log($scope.file);
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
            $timeout(function(){
              workServices.saveWork($scope.currentWork, function(response){
                $scope.works.push(response);
                $scope.init();
                $('#addWorkModal').modal("hide");
              
              });  
            },2500);
            
      
    };
    
     $scope.start = function() {
      cfpLoadingBar.start();
    };

    $scope.complete = function () {
      cfpLoadingBar.complete();
    }
    
    $scope.init();
  });
