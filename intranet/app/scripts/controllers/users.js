'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('UsersCtrl', function ($scope,userServices,workServices ,bcrypt, Upload, $timeout, $q) {
    $scope.init = function(){
      $scope.isAdd = false;
      $scope.user = {};
      $scope.selectedWork = {originalObject:{}};
      
      userServices.getUsers({},function(response){
        $scope.users = response;
      });
      workServices.getWorks({},function(response){
        $scope.works = response.response;
      });
      $scope.$watch(function(){
        $(".js-example-basic-multiple").select2();
      });     
      
      
    };
    
    
    $scope.openModal = function(e,currentUser){
      $scope.user = currentUser;
      if(currentUser == ''){
        $scope.user = {};
        $scope.isAdd = true;
      }
    };    
    
    
    $scope.changeUser = function(e){
      Upload.upload({
            url: 'http://intranet.sicosa.mx/upload/upload.php',
            data: {file: $scope.user.photo}
        }).then(function (resp) {
            var deferred = $q;
            //$scope.works.photo = resp.config.data.file.name;
            //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            $scope.photo = resp.data.tmp_name.replace('/tmp/', '');
            console.log($scope.photo);
            $scope.user.photo = $scope.photo;
            deferred.resolve();
            
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
            $timeout(function(){
              userServices.updateUser({
                id: $scope.user.id,
                first_name: $scope.user.first_name,
                last_name: $scope.user.last_name,
                photo: $scope.user.photo,
                email: $scope.user.email,
                area: $scope.user.area},function(response){
                $('#addUserModal').modal("hide");
              })
            },2500);
          
    };
    
    $scope.closeModal = function(e){
      $scope.isAdd = false;
    };
    
    $scope.saveUser = function(e){

      if(!$scope.selectedWork.originalObject.id){
        $scope.workSelect = 0;
      }else{
        $scope.workSelect = $scope.selectedWork.originalObject.id;
      }
       Upload.upload({
            url: 'http://intranet.sicosa.mx/upload/upload.php',
            data: {file: $scope.user.photo}
        }).then(function (resp) {
            var deferred = $q;
            //$scope.works.photo = resp.config.data.file.name;
            //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            $scope.photo = resp.data.tmp_name.replace('/tmp/', '');
            console.log($scope.photo);
            $scope.user.photo = $scope.photo;
            deferred.resolve();
            
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
            $timeout(function(){
              userServices.saveUser({
              first_name: $scope.user.first_name,
              last_name: $scope.user.last_name,
              email: $scope.user.email,
              photo: $scope.user.photo,
              area: $scope.user.area,
              password: bcrypt.hashSync($scope.user.password),
              works: $scope.user.works
            }, function(response){
              $scope.init();
              $('#addUserModal').modal("hide");
            });
              userServices.sendUserPass($scope.user,function(response){
                console.log(response);
              });
            },2500);
      
    };
    
    $scope.removeUser = function(e,id){
      if(confirm('Estas seguro de querer eliminar a este usuario?')){
        userServices.removeUser(id,function(response){
          for(var i=0; i<$scope.users.length; i++){
              if($scope.users[i].id == id){
                $scope.users.splice(i,1);
              }
            } 
        });
      }
    };
    
    $scope.init();
  });
