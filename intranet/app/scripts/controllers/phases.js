'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:PhasesCtrl
 * @description
 * # PhasesCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('PhasesCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
