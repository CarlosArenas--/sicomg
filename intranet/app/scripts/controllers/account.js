'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:AccountCtrl
 * @description
 * # AccountCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('AccountCtrl', function ($scope,localStorageService,$window,bcrypt,userServices,requisitionServices,workServices,spreadServices) {
    $scope.init = function(){
      userServices.getUserDetails(localStorageService.get('id'),function(response){
        $scope.user = response;
      });
      workServices.getWorks({},function(response){
        $scope.works = response.response;
      });
      userServices.getUsers({},function(response){
         $scope.users = response;
       });
      $scope.$watch(function(){
        $(".js-example-basic-multiple-users").select2();
      });
    };
    $scope.logout = function(){
         localStorageService.clearAll();
         $window.location = '#/';   
      };
      $scope.changePassword = function(e){
        if($scope.newPassword == '' || $scope.newPassword == null){
          alert("elige una contraseña");
          return 0;
        }else{
          userServices.updateUser({
                id: localStorageService.get('id'),
                password: bcrypt.hashSync($scope.newPassword)
                },function(response){
                $('#changePswModal').modal("hide");
          })
        }
      };

      $('#profile-tabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
      });

      $scope.saveReq = function(e){
        $scope.req.status = "A"; //status not approved yet
        requisitionServices.saveReq($scope.req,function(response){
          $scope.req = {};
          $("#successModal").modal('show');
        });
      };

      $scope.saveSpread = function(){
        if($scope.spread.name == '' || $scope.spread.name == null){
          alert("por favor completa el campo de título");
          return 0;
        }

        $scope.spread.work = $scope.selectedWork.originalObject.id;

        $scope.spread.author = localStorageService.get('name');
        $scope.spread.users.push(localStorageService.get('id').toString());
        $scope.spread.url = $scope.spread.name +''+Math.floor(Date.now() / 1000);
        spreadServices.saveSpread($scope.spread,function(response){
          $('#addSpread').modal("hide");
          $scope.user.spreads.push(response);
          $("#successModal").modal('show');
        });
        spreadServices.sendPush($scope.spread,function(response){

        });
      };
      $scope.init();
  });
