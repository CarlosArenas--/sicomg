'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:WorkdetailsCtrl
 * @description
 * # WorkdetailsCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('WorkdetailsCtrl', function ($scope, workServices, $routeParams) {
    $scope.init = function(){
        workServices.getWorkById($routeParams.id,function(response){
            $scope.work = response;
        });
        $(function () {
        $('#pieContainer').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        exporting: {
            enabled: false
        },
        credits:{
            enabled: false
        },
        tooltip: {
            pointFormat: ''
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Alcantarillado',
                y: 24.03
            }, {
                name: 'Albañilería',
                y: 10.38
            }, {
                name: 'Demolición',
                y: 4.77
            }]
        }]
    });
});
    };
    
    $scope.init();
    
  });
