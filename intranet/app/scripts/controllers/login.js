'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('LoginCtrl', function ($scope,$window,authService) {
    $scope.init = function(){
       
    };
    
    $scope.signme = function(e){
      if($scope.username == '' || $scope.password == '' || $scope.username == null || $scope.password == null){
          alert("Por favor llena todos los campos");
          return 0;
      }else{
          authService.login({email: $scope.username, password: $scope.password},function(response){
              if(response.token != null){
                $window.location = '#/dashboard';    
              }
              
          });
          
      }  
    };

    $scope.enter = function(e){
      if(e.keyCode == 13){
        $scope.signme();
      }
    }
    
    $scope.init();
    
  });
