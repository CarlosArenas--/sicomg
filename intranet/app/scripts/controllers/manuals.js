'use strict';

/**
 * @ngdoc function
 * @name intranetApp.controller:ManualsCtrl
 * @description
 * # ManualsCtrl
 * Controller of the intranetApp
 */
angular.module('intranetApp')
  .controller('ManualsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
