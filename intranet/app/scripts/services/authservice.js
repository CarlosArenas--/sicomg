'use strict';

/**
 * @ngdoc service
 * @name intranetApp.authService
 * @description
 * # authService
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('authService', function ($resource,localStorageService,constants,$window) {
    
    var baseUrl = constants.baseUrl;
    // Service logic
    // ...

    var login = function( params, callback ) {
        var Service = $resource(baseUrl+'login/');
	       Service.save(params).$promise.then(function(response) {
		        _loginResponse(response, callback);
		     }, _errorResponse);
    };
    
    var isLogged = function( params, callback ) {
        var Service = $resource(baseUrl+'User/'+localStorageService.get('id'));
	       Service.get(params).$promise.then(function(response) {
		        _successResponse(response, callback);
		     }, _errorResponse);
    };
    
   
    var _loginResponse = function(response, callback){
       
       console.log(response);
        localStorageService.set('token', response.token);
        localStorageService.set('id', response.user.id);
        localStorageService.set('name',response.user.first_name + ' ' + response.user.last_name );
        
        localStorageService.set('area',response.user.area);
        //localStorageService.set(user, response.user);
        return callback( response );
        
    };
    
    
    var _successResponse = function(response, callback){
        
        //localStorageService.set(user, response.user);
        return callback( response );
    };
    
    var _errorResponse = function(response){
        console.log("on error");   
        $window.location = '#/';
    }
    // Public API here
    return {
      login:login ,
      isLogged: isLogged
    };
  });
