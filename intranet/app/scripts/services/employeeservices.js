'use strict';

/**
 * @ngdoc service
 * @name intranetApp.employeeServices
 * @description
 * # employeeServices
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('employeeServices', function ($resource, localStorageService, constants) {
   var baseUrl = constants.baseUrl;
    // Service logic
    // ...

    var getEmployees = function( params, callback ) {
          var Service = $resource(baseUrl+'employee');
          Service.query().$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var saveEmployee = function( params, callback ) {
          var Service = $resource(baseUrl+'employee/saveEmployee');
          Service.save(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var updateEmployee = function(params, callback){
      var Service = $resource(baseUrl+'employee/'+params.id,null,{
                update:{ method: 'PUT',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.update(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var removeEmployee = function( params, callback ) {
          var Service = $resource(baseUrl+'employee/'+params,null,{
                remove:{ method: 'DELETE',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.remove(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    
    
   
    var _errorResponse = function(response){
      console.log('error');
    }
    var _successResponse = function(response, callback){
        return callback( response );
    };

    // Public API here
    return {
        getEmployees: getEmployees,
        saveEmployee: saveEmployee,
        updateEmployee: updateEmployee,
        removeEmployee: removeEmployee
    };
  });
