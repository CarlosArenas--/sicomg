'use strict';

/**
 * @ngdoc service
 * @name intranetApp.materialService
 * @description
 * # materialService
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('materialService', function ($resource, constants,localStorageService) {
     var baseUrl = constants.baseUrl;
    // Service logic
    // ...

    var getMaterials = function( params, callback ) {
          var Service = $resource(baseUrl+'material');
          Service.query().$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var saveMaterial = function( params, callback ) {
          var Service = $resource(baseUrl+'material');
          Service.save(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var _successResponse = function(response, callback){
        return callback( response );
    };
    var _errorResponse = function(response){
      console.log('error');
    }

    
    return{
      getMaterials: getMaterials,
      saveMaterial: saveMaterial
    }
   
  });
