'use strict';

/**
 * @ngdoc service
 * @name intranetApp.employeeServices
 * @description
 * # employeeServices
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('spreadServices', function ($resource, localStorageService, constants) {
   var baseUrl = constants.baseUrl;
    // Service logic
    // ...

    var getSpreads = function( params, callback ) {
          var Service = $resource(baseUrl+'spread');
          Service.query().$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var saveSpread = function( params, callback ) {
          var Service = $resource(baseUrl+'spread');
          Service.save(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };

    var sendPush = function( params, callback ) {
          var Service = $resource(baseUrl+'spread/sendpush');
          Service.save(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };


    
    // var updateEmployee = function(params, callback){
    //   var Service = $resource(baseUrl+'employee/'+params.id,null,{
    //             update:{ method: 'PUT',
    //             headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
    //       Service.update(params).$promise.then(function(response) {
    //       _successResponse(response, callback);
    //   }, _errorResponse);
    // };
    
    var removeSpread = function( params, callback ) {
          var Service = $resource(baseUrl+'spread/'+params,null,{
                remove:{ method: 'DELETE',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.remove(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    
    
   
    var _errorResponse = function(response){
      console.log('error');
    }
    var _successResponse = function(response, callback){
        return callback( response );
    };

    // Public API here
    return {
        getSpreads: getSpreads,
        saveSpread: saveSpread,
        sendPush:sendPush,
        // updateEmployee: updateEmployee,
         removeSpread: removeSpread
    };
  });
