'use strict';

/**
 * @ngdoc service
 * @name intranetApp.phaseServices
 * @description
 * # phaseServices
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('phaseServices', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
