'use strict';

/**
 * @ngdoc service
 * @name intranetApp.workServices
 * @description
 * # workServices
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('workServices', function ($resource,localStorageService, constants) {
    // Service logic
    // ...
    var baseUrl = constants.baseUrl;
    //var baseUrl = 'http://localhost:1337/';
    // Service logic
    // ...

    var getWorks = function( params, callback ) {
          var Service = $resource(baseUrl+'work/getWorks');
          Service.get().$promise.then(function(response) {
          _successResponse(response, callback);
      }, console.log("error"));
    };
    
    var getWorkById = function(params,callback){
          console.log(localStorageService.get('token'));
            var Service = $resource(baseUrl+'work/'+params,null,{
                query:{ method: 'GET',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.query().$promise.then(function(response) {
          _successResponse(response, callback);
      }, console.log("error"));
    };
    
    var removeWork = function(params, callback){
      var Service = $resource(baseUrl+'work/'+params,null,{
                remove:{ method: 'DELETE',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.remove(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, console.log("error"));
    };
    
    
    var updateWork = function(params, callback){
      var Service = $resource(baseUrl+'work/'+params.id,null,{
                update:{ method: 'PUT',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.update(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, console.log("error"));
    };
    
    
    var saveWork = function(params, callback){
      var Service = $resource(baseUrl+'work/',null,{
                post:{ method: 'POST',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.post(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, console.log("error"));
    };
    
  
    
    
    var _successResponse = function(response, callback){
        return callback( response );
    };

    // Public API here
    return {
        getWorks: getWorks,
        getWorkById:getWorkById,
        removeWork: removeWork,
        updateWork:updateWork,
        saveWork: saveWork
    };
  });
