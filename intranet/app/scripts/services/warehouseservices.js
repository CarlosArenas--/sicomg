'use strict';

/**
 * @ngdoc service
 * @name intranetApp.warehouseServices
 * @description
 * # warehouseServices
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('warehouseServices', function (localStorageService,constants, $resource) {
   var baseUrl = constants.baseUrl;
    // Service logic
    // ...

    var getWares = function( params, callback ) {
          var Service = $resource(baseUrl+'spread');
          Service.query().$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var saveWare = function( params, callback ) {
          var Service = $resource(baseUrl+'spread');
          Service.save(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var updateWare = function(params, callback){
      var Service = $resource(baseUrl+'employee/'+params.id,null,{
                update:{ method: 'PUT',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.update(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var removeWare = function( params, callback ) {
          var Service = $resource(baseUrl+'spread/'+params,null,{
                remove:{ method: 'DELETE',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.remove(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    
    
   
    var _errorResponse = function(response){
      console.log('error');
    }
    var _successResponse = function(response, callback){
        return callback( response );
    };

    // Public API here
    return {
        getWares: getWares,
        saveWare: saveWare,
        // updateEmployee: updateEmployee,
        updateWare: updateWare
    };
  });
