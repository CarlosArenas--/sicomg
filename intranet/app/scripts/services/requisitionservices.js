'use strict';

/**
 * @ngdoc service
 * @name intranetApp.requisitionServices
 * @description
 * # requisitionServices
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('requisitionServices', function ($resource, localStorageService, constants) {
    var baseUrl = constants.baseUrl;
     // Service logic
     // ...

     var getReqs = function( params, callback ) {
           var Service = $resource(baseUrl+'requisition');
           Service.query().$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };

     var saveReq = function( params, callback ) {
           var Service = $resource(baseUrl+'requisition');
           Service.save(params).$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };

     var updateReq = function(params, callback){
       var Service = $resource(baseUrl+'requisition/'+params.id,null,{
                 update:{ method: 'PUT',
                 headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
           Service.update(params).$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };

     var removeReq = function( params, callback ) {
           var Service = $resource(baseUrl+'requisition/'+params,null,{
                 remove:{ method: 'DELETE',
                 headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
           Service.remove(params).$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };




     var _errorResponse = function(response){
       console.log('error');
     }
     var _successResponse = function(response, callback){
         return callback( response );
     };

     // Public API here
     return {
         getReqs: getReqs,
         saveReq: saveReq,
         updateReq: updateReq,
         removeReq: removeReq
     };
  });
