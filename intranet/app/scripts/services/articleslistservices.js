'use strict';

/**
 * @ngdoc service
 * @name intranetApp.articlesListServices
 * @description
 * # articlesListServices
 * Service in the intranetApp.
 */
angular.module('intranetApp')
  .factory('articlesListServices', function ($resource, constants,localStorageService) {
  	var baseUrl = constants.baseUrl;

    var getArticles = function( params, callback ) {
           var Service = $resource(baseUrl+'article');
           Service.query().$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };

     var saveArticle = function( params, callback ) {
           var Service = $resource(baseUrl+'article');
           Service.save(params).$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };

     var updateArticle = function(params, callback){
       var Service = $resource(baseUrl+'article/'+params.id,null,{
                 update:{ method: 'PUT',
                 headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
           Service.update(params).$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };

     var removeArticle = function( params, callback ) {
           var Service = $resource(baseUrl+'article/'+params,null,{
                 remove:{ method: 'DELETE',
                 headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
           Service.remove(params).$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };

     var getArticlesUsers = function( params, callback ) {
           var Service = $resource(baseUrl+'Article/getArticlesUsers');
           Service.get().$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };

     var assignArticleToUser = function( params, callback ) {
           var Service = $resource(baseUrl+'articleuser');
           Service.save(params).$promise.then(function(response) {
           _successResponse(response, callback);
       }, _errorResponse);
     };

     var _errorResponse = function(response){
      console.log('error');
    }
    var _successResponse = function(response, callback){
        return callback( response );
    };

    return {
		    getArticles:getArticles,
        saveArticle:saveArticle,
        updateArticle:updateArticle,
        removeArticle: removeArticle,
        getArticlesUsers: getArticlesUsers,
        assignArticleToUser:assignArticleToUser
	  };
  });
