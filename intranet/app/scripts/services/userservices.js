'use strict';

/**
 * @ngdoc service
 * @name intranetApp.userServices
 * @description
 * # userServices
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('userServices', function ($resource, constants,localStorageService) {
     var baseUrl = constants.baseUrl;
    // Service logic
    // ...

    var getUsers = function( params, callback ) {
          var Service = $resource(baseUrl+'User');
          Service.query().$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };

    var getUserDetails = function( params, callback ) {
        var Service = $resource(baseUrl+'User/'+params);
	       Service.get(params).$promise.then(function(response) {
		        _successResponse(response, callback);
		     }, _errorResponse);
    };
    
    var saveUser = function( params, callback ) {
          var Service = $resource(baseUrl+'User');
          Service.save(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var updateUser = function(params, callback){
      var Service = $resource(baseUrl+'User/'+params.id,null,{
                update:{ method: 'PUT',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.update(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var removeUser = function( params, callback ) {
          var Service = $resource(baseUrl+'User/'+params,null,{
                remove:{ method: 'DELETE',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.remove(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var sendUserPass = function( params, callback ) {
          var Service = $resource(baseUrl+'User/sendUserPass');
          Service.save(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
   
    var _errorResponse = function(response){
      console.log('error');
    }
    var _successResponse = function(response, callback){
        return callback( response );
    };

    // Public API here
    return {
        getUsers: getUsers,
        getUserDetails:getUserDetails,
        saveUser: saveUser,
        updateUser: updateUser,
        removeUser: removeUser,
        sendUserPass: sendUserPass
    };
  });
