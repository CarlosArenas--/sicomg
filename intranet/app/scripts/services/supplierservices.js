'use strict';

/**
 * @ngdoc service
 * @name intranetApp.supplierServices
 * @description
 * # supplierServices
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('supplierServices', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
