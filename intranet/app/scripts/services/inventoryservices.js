'use strict';

/**
 * @ngdoc service
 * @name intranetApp.inventoryServices
 * @description
 * # inventoryServices
 * Factory in the intranetApp.
 */
angular.module('intranetApp')
  .factory('inventoryServices', function (localStorageService,constants, $resource) {
     var baseUrl = constants.baseUrl;
    // Service logic
    // ...

    var getWares = function( params, callback ) {
          var Service = $resource(baseUrl+'Warehouse/');
          Service.query().$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var saveWare = function( params, callback ) {
          var Service = $resource(baseUrl+'warehouse');
          Service.save(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var updateWare = function(params, callback){
      var Service = $resource(baseUrl+'warehouse/'+params.id,null,{
                update:{ method: 'PUT',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.update(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    var removeWare = function( params, callback ) {
          var Service = $resource(baseUrl+'warehouse/'+params,null,{
                remove:{ method: 'DELETE',
                headers:{'Authorization':'Bearer '+localStorageService.get('token')}}});
          Service.remove(params).$promise.then(function(response) {
          _successResponse(response, callback);
      }, _errorResponse);
    };
    
    
    
    var _successResponse = function(response, callback){
        return callback( response );
    };
    var _errorResponse = function(response){
      console.log('error');
    }
   

    // Public API here
    return {
        getWares: getWares,
        saveWare: saveWare,
        removeWare: removeWare,
        updateWare: updateWare
    };
  });
