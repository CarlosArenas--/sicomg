'use strict';

/**
 * @ngdoc overview
 * @name intranetApp
 * @description
 * # intranetApp
 *
 * Main module of the application.
 */
angular
  .module('intranetApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'textAngular',
    'LocalStorageModule',
    'ngMap',
    'flow',
    'angular-loading-bar',
    'cfp.loadingBar',
    'ui.pwgen',
    'dtrw.bcrypt',
    'ngFileUpload',
    'angucomplete-alt',
    'ngTable',
    'smart-table',
    'angular-toArrayFilter'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/dashboard', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main',
        resolve:{
            header: function(){
                $('.header').show();
            },
            isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;
              if(localStorageService.get('id'))
              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                    localStorageService.set('name',response.first_name + ' ' + response.last_name );
                    localStorageService.set('id', response.id);
                    localStorageService.set('area',response.area);

                    deferred.resolve();
                }else{
                    deferred.reject();
                    $window.location = '#/';
                }
                
              });

              return deferred.promise;
            }
        }
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about',
         resolve:{
            header: function(){
                $('.header').show();
            },
            isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;
            }
        }
      })
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login',
        resolve:{
            header: function($window,localStorageService,$q){
                $('.header').hide();
                if(localStorageService.get('token') != null){
                    $window.location = '#/dashboard'
                }
            },

        }
      })
      .when('/works', {
        templateUrl: 'views/works.html',
        controller: 'WorksCtrl',
        controllerAs: 'works',
        resolve:{
          isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                console.log(response);
                if(response){
                    localStorageService.set('name',response.first_name + ' ' + response.last_name );
                    localStorageService.set('id', response.id);
                    localStorageService.set('area',response.area);
                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/reports', {
        templateUrl: 'views/reports.html',
        controller: 'ReportsCtrl',
        controllerAs: 'reports',
        resolve:{
          isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/works/:id', {
        templateUrl: 'views/workdetails.html',
        controller: 'WorkdetailsCtrl',
        controllerAs: 'workDetails',
        resolve:{
          isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                  if(response.area != 'Administrador'){

                    $window.location = '#/';
                  }
                  deferred.resolve();

                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/employees', {
        templateUrl: 'views/employees.html',
        controller: 'EmployeesCtrl',
        controllerAs: 'employees',
        resolve:{
          isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                  if(response.area != 'Administrador' && response.area != 'RH' && response.area != 'Encargado de Obra'){

                    $window.location = '#/';
                  }
                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/inventory', {
        templateUrl: 'views/inventory.html',
        controller: 'InventoryCtrl',
        controllerAs: 'inventory',
        resolve:{
          isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){

                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl',
        controllerAs: 'users',
        resolve:{
          isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                  // if(response.area != 'Administrador'){
                  //   $window.location = '#/';
                  // }
                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/phases', {
        templateUrl: 'views/phases.html',
        controller: 'PhasesCtrl',
        controllerAs: 'phases',
        resolve:{
          isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/estimator', {
        templateUrl: 'views/estimator.html',
        controller: 'EstimatorCtrl',
        controllerAs: 'estimator',
         resolve:{
            header: function(){
                $('.header').show();
            },
            isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                  if(response.area != 'Administrador' && response.area != 'Estimador'){

                    $window.location = '#/';
                  }
                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/spreads', {
        templateUrl: 'views/spreads.html',
        controller: 'SpreadsCtrl',
        controllerAs: 'spreads',
        resolve:{
            header: function(){
                $('.header').show();
            },
            isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                  if(response.area != 'Administrador' && response.area != 'RH'){

                    $window.location = '#/';
                  }
                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/reportDetails', {
        templateUrl: 'views/reportdetails.html',
        controller: 'ReportdetailsCtrl',
        controllerAs: 'reportDetails'
      })
      .when('/requisitions', {
        templateUrl: 'views/requisitions.html',
        controller: 'RequisitionsCtrl',
        controllerAs: 'requisitions',
        resolve:{
            header: function(){
                $('.header').show();
            },
            isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                  if(response.area != 'Administrador' && response.area != 'RH'){

                    $window.location = '#/';
                  }
                  deferred.resolve();
                }else{
                  deferred.reject();
                  $window.location = '#/';
                }

              });

              return deferred.promise;

            }
        }
      })
      .when('/account', {
        templateUrl: 'views/account.html',
        controller: 'AccountCtrl',
        controllerAs: 'account',
        resolve:{
            header: function(){
                $('.header').show();
            },
            isLogged: function(authService,$q,$window,localStorageService){
              var deferred = $q;

              authService.isLogged(localStorageService.get('id'),function(response){
                if(response){
                    localStorageService.set('name',response.first_name + ' ' + response.last_name );
                    localStorageService.set('id', response.id);
                    localStorageService.set('area',response.area);

                    deferred.resolve();
                }else{
                    deferred.reject();
                    $window.location = '#/';
                }
                return deferred.promise;
              });

              return deferred.promise;
            }
        }
      })
      .when('/manuals', {
        templateUrl: 'views/manuals.html',
        controller: 'ManualsCtrl',
        controllerAs: 'manuals'
      })
      .when('/reports/requisition', {
        templateUrl: 'views/requisition.html',
        controller: 'RequisitionCtrl',
        controllerAs: 'requisition'
      })
      .when('/articles', {
        templateUrl: 'views/articles.html',
        controller: 'ArticlesCtrl',
        controllerAs: 'articles'
      })
      .when('/piecework', {
        templateUrl: 'views/piecework.html',
        controller: 'PieceworkCtrl',
        controllerAs: 'piecework'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = true;
  }]).constant("constants", {
        "baseUrl": "http://intranet.sicosa.mx:8088/",
        // "baseUrl": "http://localhost:1337/"
    });
