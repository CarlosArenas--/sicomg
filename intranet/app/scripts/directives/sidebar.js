'use strict';

/**
 * @ngdoc directive
 * @name intranetApp.directive:sidebar
 * @description
 * # sidebar
 */
angular.module('intranetApp')
  .directive('sidebar', function () {
    return {
      templateUrl: 'views/sidebar-directive.html',
      restrict: 'E',
      controller: function($scope,localStorageService){
        $scope.role = localStorageService.get('area');
        console.log($scope.role);
      },
      link: function postLink(scope, element, attrs) {
        //element.text('this is the sidebar directive');
      }
    };
  });
