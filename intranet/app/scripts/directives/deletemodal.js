'use strict';

/**
 * @ngdoc directive
 * @name intranetApp.directive:deleteModal
 * @description
 * # deleteModal
 */
angular.module('intranetApp')
  .directive('deleteModal', function () {
    return {
      templateUrl: 'views/deletemodal.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        // element.text('this is the deleteModal directive');
      }
    };
  });
