'use strict';

/**
 * @ngdoc directive
 * @name intranetApp.directive:newRequestArticle
 * @description
 * # newRequestArticle
 */
angular.module('intranetApp')
  .directive('newRequestArticle', function () {
    return {
      templateUrl: 'views/newrequestarticle.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        // element.text('this is the newRequestArticle directive');
      }
    };
  });
