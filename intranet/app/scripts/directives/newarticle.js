'use strict';

/**
 * @ngdoc directive
 * @name intranetApp.directive:newArticle
 * @description
 * # newArticle
 */
angular.module('intranetApp')
  .directive('newArticle', function () {
    return {
      templateUrl: 'views/newarticle.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        // element.text('this is the newArticle directive');
      }
    };
  });
