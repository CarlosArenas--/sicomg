'use strict';

describe('Directive: newRequestArticle', function () {

  // load the directive's module
  beforeEach(module('intranetApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<new-request-article></new-request-article>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the newRequestArticle directive');
  }));
});
