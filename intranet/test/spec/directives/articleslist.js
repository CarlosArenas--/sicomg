'use strict';

describe('Directive: articlesList', function () {

  // load the directive's module
  beforeEach(module('intranetApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<articles-list></articles-list>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the articlesList directive');
  }));
});
