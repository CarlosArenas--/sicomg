'use strict';

describe('Directive: requestsAricleList', function () {

  // load the directive's module
  beforeEach(module('intranetApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<requests-aricle-list></requests-aricle-list>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the requestsAricleList directive');
  }));
});
