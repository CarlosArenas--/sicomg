'use strict';

describe('Service: spreadservices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var spreadservices;
  beforeEach(inject(function (_spreadservices_) {
    spreadservices = _spreadservices_;
  }));

  it('should do something', function () {
    expect(!!spreadservices).toBe(true);
  });

});
