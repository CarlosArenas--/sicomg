'use strict';

describe('Service: warehouse', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var warehouse;
  beforeEach(inject(function (_warehouse_) {
    warehouse = _warehouse_;
  }));

  it('should do something', function () {
    expect(!!warehouse).toBe(true);
  });

});
