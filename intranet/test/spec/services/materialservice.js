'use strict';

describe('Service: materialService', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var materialService;
  beforeEach(inject(function (_materialService_) {
    materialService = _materialService_;
  }));

  it('should do something', function () {
    expect(!!materialService).toBe(true);
  });

});
