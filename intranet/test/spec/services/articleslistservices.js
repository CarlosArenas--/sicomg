'use strict';

describe('Service: articlesListServices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var articlesListServices;
  beforeEach(inject(function (_articlesListServices_) {
    articlesListServices = _articlesListServices_;
  }));

  it('should do something', function () {
    expect(!!articlesListServices).toBe(true);
  });

});
