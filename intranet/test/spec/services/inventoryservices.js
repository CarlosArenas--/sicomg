'use strict';

describe('Service: inventoryServices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var inventoryServices;
  beforeEach(inject(function (_inventoryServices_) {
    inventoryServices = _inventoryServices_;
  }));

  it('should do something', function () {
    expect(!!inventoryServices).toBe(true);
  });

});
