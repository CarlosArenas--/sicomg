'use strict';

describe('Service: supplierServices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var supplierServices;
  beforeEach(inject(function (_supplierServices_) {
    supplierServices = _supplierServices_;
  }));

  it('should do something', function () {
    expect(!!supplierServices).toBe(true);
  });

});
