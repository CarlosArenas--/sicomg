'use strict';

describe('Service: warehouseServices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var warehouseServices;
  beforeEach(inject(function (_warehouseServices_) {
    warehouseServices = _warehouseServices_;
  }));

  it('should do something', function () {
    expect(!!warehouseServices).toBe(true);
  });

});
