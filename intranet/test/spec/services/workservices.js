'use strict';

describe('Service: workServices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var workServices;
  beforeEach(inject(function (_workServices_) {
    workServices = _workServices_;
  }));

  it('should do something', function () {
    expect(!!workServices).toBe(true);
  });

});
