'use strict';

describe('Service: phaseServices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var phaseServices;
  beforeEach(inject(function (_phaseServices_) {
    phaseServices = _phaseServices_;
  }));

  it('should do something', function () {
    expect(!!phaseServices).toBe(true);
  });

});
