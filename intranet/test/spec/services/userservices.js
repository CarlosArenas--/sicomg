'use strict';

describe('Service: userServices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var userServices;
  beforeEach(inject(function (_userServices_) {
    userServices = _userServices_;
  }));

  it('should do something', function () {
    expect(!!userServices).toBe(true);
  });

});
