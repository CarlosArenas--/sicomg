'use strict';

describe('Service: employeeServices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var employeeServices;
  beforeEach(inject(function (_employeeServices_) {
    employeeServices = _employeeServices_;
  }));

  it('should do something', function () {
    expect(!!employeeServices).toBe(true);
  });

});
