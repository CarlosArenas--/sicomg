'use strict';

describe('Service: requisitionServices', function () {

  // load the service's module
  beforeEach(module('intranetApp'));

  // instantiate service
  var requisitionServices;
  beforeEach(inject(function (_requisitionServices_) {
    requisitionServices = _requisitionServices_;
  }));

  it('should do something', function () {
    expect(!!requisitionServices).toBe(true);
  });

});
