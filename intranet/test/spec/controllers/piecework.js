'use strict';

describe('Controller: PieceworkCtrl', function () {

  // load the controller's module
  beforeEach(module('intranetApp'));

  var PieceworkCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PieceworkCtrl = $controller('PieceworkCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PieceworkCtrl.awesomeThings.length).toBe(3);
  });
});
