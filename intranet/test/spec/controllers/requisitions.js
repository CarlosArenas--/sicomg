'use strict';

describe('Controller: RequisitionsCtrl', function () {

  // load the controller's module
  beforeEach(module('intranetApp'));

  var RequisitionsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RequisitionsCtrl = $controller('RequisitionsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RequisitionsCtrl.awesomeThings.length).toBe(3);
  });
});
