'use strict';

describe('Controller: SpreadsCtrl', function () {

  // load the controller's module
  beforeEach(module('intranetApp'));

  var SpreadsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SpreadsCtrl = $controller('SpreadsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SpreadsCtrl.awesomeThings.length).toBe(3);
  });
});
