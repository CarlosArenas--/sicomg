'use strict';

describe('Controller: ReportdetailsCtrl', function () {

  // load the controller's module
  beforeEach(module('intranetApp'));

  var ReportdetailsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReportdetailsCtrl = $controller('ReportdetailsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ReportdetailsCtrl.awesomeThings.length).toBe(3);
  });
});
