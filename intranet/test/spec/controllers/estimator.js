'use strict';

describe('Controller: EstimatorCtrl', function () {

  // load the controller's module
  beforeEach(module('intranetApp'));

  var EstimatorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EstimatorCtrl = $controller('EstimatorCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EstimatorCtrl.awesomeThings.length).toBe(3);
  });
});
