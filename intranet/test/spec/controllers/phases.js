'use strict';

describe('Controller: PhasesCtrl', function () {

  // load the controller's module
  beforeEach(module('intranetApp'));

  var PhasesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PhasesCtrl = $controller('PhasesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PhasesCtrl.awesomeThings.length).toBe(3);
  });
});
